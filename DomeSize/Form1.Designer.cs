﻿namespace DomeSize
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.rulerList = new System.Windows.Forms.ComboBox();
            this.calcNumText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.calcBtn = new System.Windows.Forms.Button();
            this.rulerNameLabel = new System.Windows.Forms.Label();
            this.rulerSizeNumLabel = new System.Windows.Forms.Label();
            this.rulerUnitLabel = new System.Windows.Forms.Label();
            this.resultNumLabel = new System.Windows.Forms.Label();
            this.imagePanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rulerList
            // 
            this.rulerList.FormattingEnabled = true;
            this.rulerList.Location = new System.Drawing.Point(49, 37);
            this.rulerList.Name = "rulerList";
            this.rulerList.Size = new System.Drawing.Size(205, 20);
            this.rulerList.TabIndex = 0;
            this.rulerList.SelectedIndexChanged += new System.EventHandler(this.RulerListChanged);
            // 
            // calcNumText
            // 
            this.calcNumText.Location = new System.Drawing.Point(25, 84);
            this.calcNumText.Name = "calcNumText";
            this.calcNumText.Size = new System.Drawing.Size(254, 19);
            this.calcNumText.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.Location = new System.Drawing.Point(25, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 54);
            this.button1.TabIndex = 2;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button2.Location = new System.Drawing.Point(88, 141);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 54);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button4.Location = new System.Drawing.Point(151, 213);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(57, 54);
            this.button4.TabIndex = 2;
            this.button4.Text = "6";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button5.Location = new System.Drawing.Point(88, 213);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(57, 54);
            this.button5.TabIndex = 2;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button6.Location = new System.Drawing.Point(25, 213);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(57, 54);
            this.button6.TabIndex = 2;
            this.button6.Text = "4";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button7.Location = new System.Drawing.Point(25, 285);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(57, 54);
            this.button7.TabIndex = 2;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button8.Location = new System.Drawing.Point(88, 285);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(57, 54);
            this.button8.TabIndex = 2;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button9.Location = new System.Drawing.Point(151, 285);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(57, 54);
            this.button9.TabIndex = 2;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button10.Location = new System.Drawing.Point(25, 357);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(57, 54);
            this.button10.TabIndex = 2;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button11.Location = new System.Drawing.Point(88, 357);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(57, 54);
            this.button11.TabIndex = 2;
            this.button11.Text = "00";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button13.Location = new System.Drawing.Point(151, 141);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(57, 54);
            this.button13.TabIndex = 2;
            this.button13.Text = "3";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button14.Location = new System.Drawing.Point(151, 357);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(57, 54);
            this.button14.TabIndex = 2;
            this.button14.Text = "000";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // clearBtn
            // 
            this.clearBtn.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.clearBtn.Location = new System.Drawing.Point(222, 141);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(57, 54);
            this.clearBtn.TabIndex = 2;
            this.clearBtn.Text = "C";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.ClearBtnClick);
            // 
            // calcBtn
            // 
            this.calcBtn.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.calcBtn.Location = new System.Drawing.Point(222, 285);
            this.calcBtn.Name = "calcBtn";
            this.calcBtn.Size = new System.Drawing.Size(57, 126);
            this.calcBtn.TabIndex = 2;
            this.calcBtn.Text = "計算";
            this.calcBtn.UseVisualStyleBackColor = true;
            this.calcBtn.Click += new System.EventHandler(this.CalcBtnClick);
            // 
            // rulerNameLabel
            // 
            this.rulerNameLabel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerNameLabel.Location = new System.Drawing.Point(319, 285);
            this.rulerNameLabel.Name = "rulerNameLabel";
            this.rulerNameLabel.Size = new System.Drawing.Size(195, 45);
            this.rulerNameLabel.TabIndex = 3;
            this.rulerNameLabel.Text = "東京ドーム";
            this.rulerNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rulerSizeNumLabel
            // 
            this.rulerSizeNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerSizeNumLabel.Location = new System.Drawing.Point(361, 341);
            this.rulerSizeNumLabel.Name = "rulerSizeNumLabel";
            this.rulerSizeNumLabel.Size = new System.Drawing.Size(89, 23);
            this.rulerSizeNumLabel.TabIndex = 4;
            this.rulerSizeNumLabel.Text = "???";
            this.rulerSizeNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rulerUnitLabel
            // 
            this.rulerUnitLabel.AutoSize = true;
            this.rulerUnitLabel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerUnitLabel.Location = new System.Drawing.Point(456, 351);
            this.rulerUnitLabel.Name = "rulerUnitLabel";
            this.rulerUnitLabel.Size = new System.Drawing.Size(18, 12);
            this.rulerUnitLabel.TabIndex = 5;
            this.rulerUnitLabel.Text = "㎡";
            // 
            // resultNumLabel
            // 
            this.resultNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.resultNumLabel.Location = new System.Drawing.Point(510, 324);
            this.resultNumLabel.Name = "resultNumLabel";
            this.resultNumLabel.Size = new System.Drawing.Size(73, 39);
            this.resultNumLabel.TabIndex = 6;
            this.resultNumLabel.Text = "??";
            this.resultNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imagePanel
            // 
            this.imagePanel.Location = new System.Drawing.Point(296, 27);
            this.imagePanel.Name = "imagePanel";
            this.imagePanel.Size = new System.Drawing.Size(480, 240);
            this.imagePanel.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(608, 324);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 40);
            this.label1.TabIndex = 8;
            this.label1.Text = "個分";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imagePanel);
            this.Controls.Add(this.resultNumLabel);
            this.Controls.Add(this.rulerUnitLabel);
            this.Controls.Add(this.rulerSizeNumLabel);
            this.Controls.Add(this.rulerNameLabel);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.calcBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.calcNumText);
            this.Controls.Add(this.rulerList);
            this.Name = "Form1";
            this.Text = "換算アプリ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox rulerList;
        private System.Windows.Forms.TextBox calcNumText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button calcBtn;
        private System.Windows.Forms.Label rulerNameLabel;
        private System.Windows.Forms.Label rulerSizeNumLabel;
        private System.Windows.Forms.Label rulerUnitLabel;
        private System.Windows.Forms.Label resultNumLabel;
        private System.Windows.Forms.FlowLayoutPanel imagePanel;
        private System.Windows.Forms.Label label1;
    }
}

