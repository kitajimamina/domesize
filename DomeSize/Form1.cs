﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DomeSize
{
    public partial class Form1 : Form
    {
        //基準値ルーラーリスト
        List<Ruler> rulers = new List<Ruler>();

        string imgSrc;　//ルーラー画像ソース
        string InputNumString;　//入力された数字文字列
        int rulerNum;　//ルーラー基準値

        public Form1()
        {
            InitializeComponent();
            this.rulers = new List<Ruler>();

            //ファイルからデータを読み込む
            ReadFromFile();
        }

        private void ReadFromFile()
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@"..\..\data.txt"))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');

                    int id = int.Parse (data[0]);
                    string name = data[1];
                    int sizeNum = int.Parse (data[2]);
                    string unit = data[3];
                    string ImgSrc = data[4];

                    rulers.Add(new Ruler(id, name, sizeNum, unit, ImgSrc));
                }
            }
        }

        //(基準値)Rulerインスタンスの読み込み
        public void LoadRulerList()
        {
            //ルーラーインスタンスをリストに追加
            rulers.Add(new Ruler(0, "東京ドーム", 46755, "㎡", "1.png"));
            rulers.Add(new Ruler(1, "エレン・イェーガー", 17, "m", "2.png"));
            rulers.Add(new Ruler(2, "富士山", 3376, "m", "3.png"));

            //コンボボックスに表示
            foreach (Ruler r in rulers)
            {
                this.rulerList.Items.Add(r.Name);
            }
        }
        //ルーラー情報を表示
        private void ShowRulerInfo(int index)
        {
            //基準値を指定
            rulerNum = rulers[index].SizeNum;

            //各ラベルに値表示
            rulerNameLabel.Text = rulers[index].Name;
            rulerSizeNumLabel.Text = rulerNum.ToString();
            rulerUnitLabel.Text = rulers[index].Unit;
            imgSrc = rulers[index].ImgSrc;
        }

        //複数のルーラー画像を表示
        private void ShowPictureBox(int resultImageNum)
        {
            //複数のルーラー画像をまとめるリスト
            List<PictureBox> pictures = new List<PictureBox>();

            for (int i = 0; i < resultImageNum; i++)
            {
                //画像コントロールのインスタンス作成
                pictures.Add(new PictureBox());

                //プロパティ設定
                pictures[i].Name = "pictureBox" + i.ToString();
                pictures[i].SizeMode = PictureBoxSizeMode.StretchImage;

                //画像ファイルを読み込んで、Imageオブジェクトとして取得する
                pictures[i].Image = Image.FromFile(@"..\..\images\" + imgSrc);
                pictures[i].Width = 64;
                pictures[i].Height = 64;
                pictures[i].Left = 64;

                //コントロールをフォームに追加
                this.imagePanel.Controls.Add(pictures[i]);
            }
        }
 
        //情報リセット
        private void ResetRulerInfo()
        {
            //計算数値をリセット
            InputNumString = "";
            calcNumText.Text = InputNumString;
            resultNumLabel.Text = "?";

            //パネル内画像を後ろから全削除
            for (int i = this.imagePanel.Controls.Count - 1; 0 <= i; i--)
            {
                this.imagePanel.Controls[i].Dispose();
            }
        }

        //フォーム読み込み時
        private void Form1_Load(object sender, EventArgs e)
        {
            ResetRulerInfo();
            //基準値リストを読み込み
            LoadRulerList();
            int index = 0;　//ルーラーリスト初期値
            this.rulerList.SelectedIndex = index;
            ShowRulerInfo(index);
        }

        //数値ボタンクリック時
        private void NumButtonClick(object sender, EventArgs e)
        {
            //senderの詳しい情報を取り扱えるようにする
            Button btn = (Button)sender;

            //押されたボタンの数字
            string btnText = btn.Text;

            //[入力された数字]に連結する
            InputNumString += btnText;

            //画面上に数字を出す
            calcNumText.Text = InputNumString;
        }

        //クリアボタンクリック時
        private void ClearBtnClick(object sender, EventArgs e)
        {
            ResetRulerInfo();
        }

        //計算ボタンクリック時
        private void CalcBtnClick(object sender, EventArgs e)
        {
            //文字列を数値変換
            int value;　//TryParse用変数
            int inputNum;　//数値変換した値
            double resultNum;　//計算結果

            bool isNumber = int.TryParse(calcNumText.Text, out value);
            if (isNumber)
            {
                inputNum = value;
            }
            else
            {
                inputNum = 0;
            }
            //ルーラーを基準に計算してテキストボックスに表示
            resultNum = inputNum / rulerNum;
            resultNumLabel.Text = resultNum.ToString();

            //結果画像表示数を制限
            int resultImageNum = (int)resultNum;
            if (resultImageNum >= 18)
            {
                resultImageNum = 18;
            }
            //ルーラー画像を表示
            ShowPictureBox(resultImageNum);
        }

        private void RulerListChanged(object sender, EventArgs e)
        {
            //情報リセット
            ResetRulerInfo();

            //選択したルーラーリストの情報を表示
            int index = this.rulerList.SelectedIndex;
            ShowRulerInfo(index);
        }
    }
}
