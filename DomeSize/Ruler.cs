﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomeSize
{
    //基準ものさしクラス
    class Ruler
    {
        private int id;　//基準番号
        private string name;　//基準名
        private int sizeNum;　//基準値
        private string unit;　//基準単位
        private string imgSrc; //基準画像

        //コンストラクタ自動生成
        public Ruler(int id, string name, int sizeNum, string unit, string imgSrc)
        {
            this.Id = id;
            this.Name = name;
            this.SizeNum = sizeNum;
            this.Unit = unit;
            this.ImgSrc = imgSrc;
        }

        //ゲッター・セッター自動生成

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int SizeNum { get => sizeNum; set => sizeNum = value; }
        public string Unit { get => unit; set => unit = value; }
        public string ImgSrc { get => imgSrc; set => imgSrc = value; }

    }
}
